Spam Check

1. Instructions

Download the source to a directory.

Configure Apache to point to the directory.

Use "loader.rb" to train your spam filter. Spam goes in a spam folder, ham in ham, but they need unique directories.

Navigate to the page, enter text in the field, click the submit button.

2. Known Issues

There is no filtering in the loader. Malformed strings will crash it.

An uninitialized object cauaes index.cgi to have an error.

3. Notes

The Ankusa files originate here: https://github.com/bmuller/ankusa.

Modifications were made to run them on my local machine. I am not their creator.
