#!/usr/bin/ruby

require 'cgi'
require './ankusa'
require './ankusa/file_system_storage'

cgi = CGI.new("html4")
text = cgi['string']

def checkSpam(text)
  val = ""
  # Connect to database
  storage = Ankusa::FileSystemStorage.new 'training/corpus'
  # Initialize object
  c = Ankusa::KLDivergenceClassifier.new storage
  
  if text.nil?
    val = "Enter text..."
  else
    # Test for spam
    val = c.classify text
  end
  
  storage.close
  
  return val
end

cgi.out{
   cgi.html{
      cgi.head{ "\n"+cgi.title{"Test for Spam"} } +
      cgi.body{ "\n"+
         cgi.form{"\n"+
            cgi.h2 { checkSpam(text) } +
            cgi.hr +
            cgi.h1 { "Enter a chunk of text:" } + "\n"+
            cgi.textarea("string") +"\n"+
            cgi.br +
            cgi.submit
         }
      }
   }
}