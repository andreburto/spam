#/usr/bin/env ruby
#####
# Include the local Ankusa libraies
require './ankusa'
require './ankusa/file_system_storage'

# Reader is used to read in and store the ham/span text files from
# ENRON emails.
class Reader
  def initialize()
    @filez = Array[]
  end
  
  # Return the length of the file list
  def readLen()
    return @filez.length()
  end
  
  # Return the file path to an index
  def readVal(idx)
    return @filez.at(idx)
  end
  
  # Recursively scan directories
  def scanDir(path)
    Dir.glob("#{path}/*").each do |f|
      if File.directory? f then
        if not [".", ".."].include?(f)
          self.scanDir("#{f}")
        end
      else
        @filez.push("#{f}")
      end
    end
  end
end

# Load spam
spam = Reader.new
spam.scanDir("/home/andrew/Downloads/spam")

# Load ham
ham = Reader.new
ham.scanDir("/home/andrew/Downloads/ham")

# Connect to database
storage = Ankusa::FileSystemStorage.new 'training/corpus'
# Initialize object
c = Ankusa::NaiveBayesClassifier.new storage

# Read in the spam
for n in 0..spam.readLen-1
  puts "Loading spam #{n}"
  c.train :spam, File.read(spam.readVal(n))
  storage.save
end


# Read in the ham
for n in 0..ham.readLen-1
  puts "Loading ham #{n}"
  c.train :ham, File.read(ham.readVal(n))
  storage.save
end

# Clean things up
storage.close